-- Update rental duration and rental rates of the film
UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE title = 'The Fast and The Furious';

-- Update personal data of a customer with at least 10 rental and payment records
UPDATE customer
SET first_name = 'Sardorbek', last_name = 'Tukhtasinov', email = 'Sardorbek_Tukhtasinov@student.itpu.uz', address_id = 1, create_date = now()
WHERE customer_id IN (
    SELECT customer.customer_id
    FROM customer
    INNER JOIN payment ON customer.customer_id = payment.customer_id
    INNER JOIN rental ON payment.rental_id = rental.rental_id
    GROUP BY customer.customer_id
    HAVING COUNT(DISTINCT payment.payment_id) >= 10 AND COUNT(DISTINCT rental.rental_id) >= 10
    limit 1
);

